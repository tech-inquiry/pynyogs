# A simple scraper for the monthly-updated list of New York State procurement
# contracts at
#
#  https://online.ogs.ny.gov/purchase/spg/pdfdocs/StatewideTC.pdf
#
# The contract descriptions are scraped from the ~50 page PDF using Tabula
# and then exported to a CSV in a format suitable for import into a Postgres
# database. A list of unique contractor names is also exported to a JSON file.
#
# The listed contract periods are split into the start and end dates and then
# converted into a datetime object.
import csv
import json
import pandas as pd
import tabula

# The PDF of procurement data we will scrape.
NY_OGS_PDF = "https://online.ogs.ny.gov/purchase/spg/pdfdocs/StatewideTC.pdf"

# Get the list of dataframes -- one for each page.
dfs = tabula.read_pdf(
    NY_OGS_PDF,
    pages='all',
    pandas_options={'header': None})

# Build up a list of post-processed dataframes for each page.
df_subsets = []
for df in dfs:
    # Remove the subset of MB, WB, and SB columns that were active on
    # this page.
    df_piece = df.iloc[:, [0,1,3] + [-2,-1]]
    df_piece.columns = [
        'group_number',
        'description',
        'award_number',
        'contract_number',
        'contractor'
    ]

    # Convert the group number column into integers.
    df_piece['group_number'].fillna(-1, inplace=True)
    df_piece.loc[:, ['group_number']] = df_piece['group_number'].round().astype('Int32')

    # Convert the award number column into integers.
    df_piece['award_number'].fillna(-1, inplace=True)
    df_piece.loc[:, ['award_number']] = df_piece['award_number'].round().astype('Int32')

    # Clean up the whitespace in the contractor column.
    df_piece['contractor'] = df_piece['contractor'].str.strip()
    df_piece.replace({'contractor': r'\s+'}, value=' ', inplace=True, regex=True)

    # Split up the period column into the start and end dates.
    df_period = df.iloc[:, [2]]
    df_period.columns = ['period']
    df_boundaries = df_period['period'].str.split(' - ', expand=True)
    df_boundaries.columns = ['start_date', 'end_date']
    df_boundaries['start_date'] = pd.to_datetime(df_boundaries['start_date'])
    df_boundaries['end_date'] = pd.to_datetime(df_boundaries['end_date'])

    # Form the post-processed dataframe for this page.
    df_subset = pd.concat([df_piece, df_boundaries], axis=1)
    df_subsets.append(df_subset)

# Combine the dataframe from each page into a single dataframe.
df = pd.concat(df_subsets, ignore_index=True)

# Remove redundant rows.
duplicated = df[[
    'group_number', 'description', 'award_number', 'contract_number', 'contractor',
    'start_date', 'end_date'
]].duplicated(keep='first')
num_dropped = sum(duplicated)
print('Dropping {} redundant rows.'.format(num_dropped))
df = df.drop(df[duplicated].index)

# Write out the dataframe in a CSV format suitable for Postgres import.
df.to_csv('us_ny_ogs_filing.csv',
    index=False,
    escapechar='\\',
    quotechar='"',
    doublequote=False,
    sep='\t',
    quoting=csv.QUOTE_ALL)

# Write out the unique contractor names.
unique_names = sorted([x.lower() for x in df['contractor'].unique()])
with open('unique_names.json', 'w') as outfile:
    json.dump(unique_names, outfile, indent=1)
